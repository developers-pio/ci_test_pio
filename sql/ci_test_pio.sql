-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 25, 2022 at 07:17 PM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_test_pio`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `qty` tinyint(3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `user_id`, `product_id`, `qty`, `created_at`, `updated_at`) VALUES
(28, 4, 1, 3, '2022-03-25 12:44:12', '0000-00-00 00:00:00'),
(29, 5, 4, 3, '2022-03-25 12:44:12', '2022-03-31 13:37:39'),
(30, 4, 3, 1, '2022-03-25 12:44:12', '2022-03-26 13:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `price` float(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `image`, `status`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Product 1', 'Product desc', 'prod.jpg', 1, 99.99, '2022-03-25 11:33:05', '2022-03-25 11:32:58'),
(2, 'Product 2', 'Product desc', 'prod.jpg', 1, 80.99, '2022-03-25 11:33:05', '2022-03-25 11:32:58'),
(3, 'Product 3', 'Product desc', 'prod.jpg', 1, 1000.00, '2022-03-25 11:33:05', '2022-03-25 11:32:58'),
(4, 'Product 4', 'Product desc 4', 'prod4.jpg', 1, 500.00, '2022-03-25 11:33:05', '2022-03-25 11:32:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1 - admin, 2 - user',
  `verification_token` varchar(255) DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `role`, `verification_token`, `verified`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Test', 'admin@admin.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, NULL, 1, 1, '2022-03-25 08:33:52', '2022-03-25 08:37:06'),
(2, 'Test', 'User', 'test@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 2, NULL, 1, 1, '2022-03-25 08:33:52', '2022-03-25 08:37:06'),
(4, 'Test2', 'user', 'test1@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 2, NULL, 1, 1, '2022-03-25 10:59:48', '2022-03-25 13:43:25'),
(5, 'Test3', 'user', 'test2@test.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 2, NULL, 1, 1, '2022-03-25 11:15:10', '2022-03-25 13:43:30'),
(6, 'Test4', 'user', 'tets@dsd.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 2, NULL, 0, 1, '2022-03-25 11:19:19', '2022-03-25 13:43:41');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `cart_items_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `cart_items_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
