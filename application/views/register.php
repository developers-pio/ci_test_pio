
<div class="container"> 
<?php echo $this->session->flashdata('error'); ?>

<?php echo form_open(null,['class'=>'mt-5','method'=>'post','novalidate'=>true]); ?>

  <!-- First Name input -->
  <div class="form-outline mb-4">
  <label class="form-label" for="form2Example1">First Name</label>
    <?php echo form_input(['id'=>'form2Example1','class'=>'form-control','type'=>'text','name'=>'first_name','value'=>set_value('first_name')]) ?> 
    <?php echo form_error('first_name');?> 
  </div>

  <!-- Last Name input -->
  <div class="form-outline mb-4">
  <label class="form-label" for="form2Example1">Last Name</label>
    <?php echo form_input(['id'=>'form2Example1','class'=>'form-control','type'=>'text','name'=>'last_name','value'=>set_value('last_name')]) ?> 
    <?php echo form_error('last_name');?> 
  </div>


  <!-- Email input -->
  <div class="form-outline mb-4">
    <label class="form-label" for="form2Example1">Email address</label>
    <?php echo form_input(['id'=>'form2Example1','class'=>'form-control','type'=>'email','name'=>'email','value'=>set_value('email')]) ?> 
    <?php echo form_error('email');?> 
  </div>

  <!-- Password input -->
  <div class="form-outline mb-4">
    <label class="form-label" for="form2Example2">Password</label>
    <?php echo form_password(['id'=>'form2Example1','class'=>'form-control','name'=>'password']) ?> 
    <?php echo form_error('password');?> 
  </div> 

  <!-- Confirm Password input -->
  <div class="form-outline mb-4">
    <label class="form-label" for="form2Example2">Confirm Password</label>
    <?php echo form_password(['id'=>'form2Example1','class'=>'form-control','name'=>'conf_password']) ?> 
    <?php echo form_error('conf_password');?> 
  </div> 

  <!-- Submit button -->
  <button type="submit" class="btn btn-primary btn-block mb-4">Register</button>
 
<?php echo form_close(); ?>
</div>