
<div class="container"> 
<?php echo $this->session->flashdata('error'); ?>
<?php echo $this->session->flashdata('success'); ?>
 <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Title</th>
      <th scope="col">Description</th>
      <th scope="col">Price</th>
      <th scope="col">Action</th> 
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($products)){ ?>
      <?php foreach($products as $row) { ?>
      <tr>
        <th scope="row"><?php echo $row->id; ?></th>
        <td><?php echo $row->title; ?></td>
        <td><?php echo $row->description; ?></td>
        <td><?php echo $row->price; ?></td>
        <td>
          <?php echo form_open('products/addtocart',['method'=>'post']); ?>
            <input type="number" step="1" placeholder="qty" name="qty" value="<?php echo !empty($row->total_qty) ? $row->total_qty : 1; ?>" min="1" required>
            <?php echo form_hidden('product_id',$row->id);?>
              <button type="submit" name="action" value="add" class="btn <?php echo !empty($row->total_qty) ? 'btn-warning' : 'btn-success'; ?>"><?php echo !empty($row->total_qty) ? 'Update Cart' : 'Add To Cart'; ?></button>
            <?php if(!empty($row->total_qty)){  ?>
              <button type="submit" name="action" value="remove" class="btn btn-danger">Remove From Cart</button>
            <?php } ?>
          <?php echo form_close(); ?>
        </td>
      </tr>
      <?php } ?>
    <?php } else{ ?>
      <tr colspan="4"> 
        no products available
    </tr>
    <?php } ?> 
  </tbody>
</table>
</div>