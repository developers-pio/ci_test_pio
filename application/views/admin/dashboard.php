<div class="container"> 
<div class="row mt-5">
  <div class="col-md-3">
      <div class="card border-dark mb-3">
        <div class="card-header">Count of all active and verified users.</div>
        <div class="card-body text-dark">
          <h5 class="card-title"><?php echo $total_active_verified_users; ?></h5> 
        </div>
      </div>
    </div> 


     <div class="col-md-3">
    <div class="card border-dark mb-3">
      <div class="card-header">Count of all active and verified users attached active  products.</div>
      <div class="card-body text-dark">
        <h5 class="card-title"><?php echo $total_active_verified_users_withproducts; ?></h5> 
      </div>
    </div>
    </div> 

      <div class="col-md-3">
    <div class="card border-dark mb-3">
      <div class="card-header">all active products</div>
      <div class="card-body text-dark">
        <h5 class="card-title"><?php echo $total_active_products; ?></h5> 
      </div>
    </div>
    </div>
  

    <div class="col-md-3">
    <div class="card border-dark mb-3">
      <div class="card-header">active products which don't belong to any user</div>
      <div class="card-body text-dark">
        <h5 class="card-title"><?php echo $total_active_products_notaddedcart; ?></h5> 
      </div>
    </div>
    </div>

    <div class="col-md-3">
    <div class="card border-dark mb-3">
      <div class="card-header">Amount of all active attached products</div>
      <div class="card-body text-dark">
        <h5 class="card-title"><?php echo $total_active_products_amount; ?></h5> 
      </div>
    </div>
    </div>

    <div class="col-md-3">
    <div class="card border-dark mb-3">
      <div class="card-header">Summarized price of all active attached products</div>
      <div class="card-body text-dark">
        <h5 class="card-title"><?php echo $summerized_price_attached_products; ?></h5> 
      </div>
    </div>
    </div>

     <div class="col-md-3">
    <div class="card border-dark mb-3">
      <div class="card-header">Summarized price of all active attached products </div>
      <div class="card-body text-dark">
        <h5 class="card-title">
          <?php if(!empty($summerized_price_attached_products_users)) {  ?>
            <?php foreach($summerized_price_attached_products_users as $val) {  
                echo $val->name.'- $'.$val->price.'</br>';
              } ?>
          <?php  } ?>
        </h5> 
      </div>
    </div>
    </div>
    
</div>

</div>
