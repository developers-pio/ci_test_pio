
<div class="container"> 
<?php echo $this->session->flashdata('error'); ?>
<?php echo form_open(null,['class'=>'mt-5','method'=>'post','novalidate'=>true]); ?>
  <!-- Email input -->
  <div class="form-outline mb-4">
    <?php echo form_input(['id'=>'form2Example1','class'=>'form-control','type'=>'email','name'=>'email','value'=>set_value('email')]) ?> 
    <?php echo form_error('email');?>
    <label class="form-label" for="form2Example1">Email address</label>
  </div>

  <!-- Password input -->
  <div class="form-outline mb-4">
    <?php echo form_password(['id'=>'form2Example1','class'=>'form-control','name'=>'password']) ?> 
    <?php echo form_error('password');?>
    <label class="form-label" for="form2Example2">Password</label>
  </div> 

  <!-- Submit button -->
  <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>
 
<?php echo form_close(); ?>
</div>