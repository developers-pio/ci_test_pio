
<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Model{

    public function login($email,$password, $role = 2){ 
        $sql = $this->db->select('id,first_name,last_name, email, role, verified, status')->get_where('users',['email'=>$email,'password'=>do_hash($password),'role'=>$role]); 
        if($sql->num_rows() > 0){
            return $sql->row();
        }else{
            return false;
        }
    } 

    public function register($data){
        $sql = $this->db->insert('users',$data);
        if($sql){
            return true;
        }else{
            return false;
        }
    } 

}