
<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Product extends CI_Model{

    public function listing($userid){ 
        if(!empty($userid)){
            $sql = $this->db->select('products.*,cart_items.qty as total_qty')
                        ->where(['status'=>1])
                         ->join('cart_items','cart_items.product_id = products.id AND cart_items.user_id = '.$userid,'LEFT')
                         ->get('products'); 
        }else{
            $sql = $this->db->select('products.*')->where(['status'=>1])->get('products'); 
        }
        if($sql->num_rows() > 0){
            return $sql->result();
        }else{
            return false;
        }
    }  

}