
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public $userData;

	function __construct(){
		parent::__construct();
		$this->userData = $this->session->userdata('userData'); 
		$this->load->model('User');
	}

	/** 
	 * Front user login 
	 */
	public function login()
	{ 	
		if(!empty($this->userData)){
			redirect('/');
		}
		if($this->input->post()){
			 $this->load->library('form_validation');
			 $this->form_validation->set_rules('email','Email','required|valid_email');
			 $this->form_validation->set_rules('password','Password','required|min[6]'); 
			 $password = $this->input->post('password');
			 if($this->form_validation->run() === true){ 
				 $user = $this->User->login($this->input->post('email'),$password, 2);
				 if($user === false){
					 $this->session->set_flashdata('error','User not found.');
					 redirect('auth/login');
				 }else{
					if(!$user->verified){
						$this->session->set_flashdata('error','User not verified.');
						redirect('auth/login');
					}else if(!$user->status){
						$this->session->set_flashdata('error','User is inactive.');
						redirect('auth/login');
					}else{
						$this->session->set_userdata('userData',(array)$user);
						$this->session->set_flashdata('success','User successfully login.');
						redirect('/');
					}
				 }
			 } 
		}
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	} 

	/** 
	 * Front user register 
	 */
	public function register()
	{ 	
		if(!empty($this->userData)){
			redirect('/');
		}
		if($this->input->post()){
			 $this->load->library('form_validation');
			 $this->form_validation->set_rules('first_name','First Name','required|max_length[255]');
			 $this->form_validation->set_rules('last_name','Last Name','required|max_length[255]');
			 $this->form_validation->set_rules('email','Email','required|valid_email|callback__checkuserexist');
			 $this->form_validation->set_rules('password','Password','required|min_length[6]'); 
			 $this->form_validation->set_rules('conf_password','Confirm Password','required|min_length[6]|matches[password]'); 
			 $password = $this->input->post('password');
			 if($this->form_validation->run() === true){ 
				 $data = array(
					 'first_name'=>$this->input->post('first_name'),
					 'last_name'=>$this->input->post('last_name'),
					 'email'=>$this->input->post('email'),
					 'password'=>do_hash($this->input->post('password')),
					 'role'=>2,
					 'verified'=>0,
					 'status'=>1,
					 'verification_token'=> random_string('alnum', 16),
					 'created_at'=>date('Y-m-d H:i:s')
				 ); 
				 $user = $this->User->register($data);
				 if($user === false){
					 $this->session->set_flashdata('error','User not registered.');
					 redirect('auth/register');
				 }else{  
					$this->sendVerificationEmailToCustomer($data['email'],$data['verification_token']); 
					$this->session->set_flashdata('success','User successfully registered.');
					redirect('auth/login'); 
				 }
			 } 
		}
		$this->load->view('header');
		$this->load->view('register');
		$this->load->view('footer');
	} 


	public function verify_account($verify_code){
        if(!empty($verify_code)){
            $sql = $this->db->select('id')->get_where('users',['verification_token'=>$verify_code]);
            if($sql->num_rows() > 0){
				$this->db->update('users',['verification_token'=>null,'verified'=>'1']);
				$this->session->set_flashdata('success','User successfully verified successfully.');
			    redirect('auth/login'); 
            }
		}    
		$this->session->set_flashdata('error','Link has been expired.');
	    redirect('auth/login'); 
    }

    /** Send verification email to customer */
    protected function sendVerificationEmailToCustomer($email,$verify_code){
        $this->load->library('email'); 
        $this->email->from('your@example.com', 'Your Name');
        $this->email->to($email); 
        $this->email->subject('Account Registered');
        $url = site_url('auth/verify_account/'.$verify_code);
        $message = 'Your account has been created successfully. Please click on below link for verify you account'.$url;
        $this->email->message($message); 
        $this->email->send();
	}
	
	/** check user name exist */
	public function _checkuserexist($str)
	{
		if ($str != "")
		{
			$sql = $this->db->select('email')->get_where('users',['email'=>$str]);
			if($sql->num_rows() >0 ){ 
				$this->form_validation->set_message('_checkuserexist', 'The {field} field already exist');
				return FALSE;
			} 
		}
	 	return TRUE;
		 
	}
}
