
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public $adminData;

	function __construct(){
		parent::__construct();
		$this->adminData = $this->session->userdata('adminData'); 
	}

	/** 
	 * Admin login 
	 */
	public function login()
	{ 	
		if(!empty($this->adminData)){
			redirect('admin/dashboard');
		}
		if($this->input->post()){
			 $this->load->library('form_validation');
			 $this->form_validation->set_rules('email','Email','required|valid_email');
			 $this->form_validation->set_rules('password','Password','required|min_length[6]'); 
			 $password = $this->input->post('password');
			 if($this->form_validation->run() === true){
				 $this->load->model('User');
				 $user = $this->User->login($this->input->post('email'),$password, 1);
				 if($user === false){
					 $this->session->set_flashdata('error','Admin not found.');
					 redirect('admin/auth/login');
				 }else{
					if(!$user->verified){
						$this->session->set_flashdata('error','Admin not verified.');
						redirect('admin/auth/login');
					}else if(!$user->status){
						$this->session->set_flashdata('error','Admin is inactive.');
						redirect('admin/auth/login');
					}else{
						$this->session->set_userdata('adminData',(array)$user);
						$this->session->set_flashdata('success','Admin successfully login.');
						redirect('admin/dashboard');
					}
				 }
			 } 
		}
		$this->load->view('admin/header');
		$this->load->view('admin/login');
		$this->load->view('admin/footer');
	} 
}
