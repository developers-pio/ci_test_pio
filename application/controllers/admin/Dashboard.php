
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public $adminData;
	public $viewData;

	function __construct(){
		parent::__construct();
		$this->adminData = $this->session->userdata('adminData');
		if(empty($this->adminData)){
			redirect('admin/auth/login');
		}
	}

	/** 
	 * Admin Dashboard
	 */
	public function index()
	{ 
		$this->viewDate['total_active_verified_users'] = $this->db->where(['status'=>1,'verified'=>1])->count_all_results('users'); 
		$this->viewDate['total_active_verified_users_withproducts'] = $this->db->where(['users.status'=>1,'users.verified'=>1])->join('cart_items', 'cart_items.user_id = users.id')->count_all_results('users');  
		$this->viewDate['total_active_products'] = $this->db->where(['status'=>1])->count_all_results('products'); 
		$this->viewDate['total_active_products_notaddedcart'] = $this->db->where(['products.status'=>1,'cart_items.id IS NULL'=>NULL])->join('cart_items', 'cart_items.product_id = products.id','LEFT')->count_all_results('products'); 
		$total_active_products_amount = $this->db->select('sum(price) as total_price')->where(['status'=>1])->get('products'); 
		$this->viewDate['total_active_products_amount'] = $total_active_products_amount->num_rows() > 0 ? $total_active_products_amount->row()->total_price : 0;
		
		$summerized_price_attached_products = $this->db->select('sum(products.price*cart_items.qty) as total_price')->join('products', 'products.id = cart_items.product_id')->get('cart_items');  
		$this->viewDate['summerized_price_attached_products'] = $summerized_price_attached_products->num_rows() > 0 ? $summerized_price_attached_products->row()->total_price : 0;
		
		$summerized_price_attached_products_users = $this->db->select('sum(products.price) as price, CONCAT_WS(" ",first_name,last_name) as name',false)->join('products', 'products.id = cart_items.product_id')->join('users', 'users.id = cart_items.user_id')->group_by('users.id')->get('cart_items');  
		$this->viewDate['summerized_price_attached_products_users'] = $summerized_price_attached_products_users->num_rows() > 0 ? $summerized_price_attached_products_users->result() : '';
		 
		$this->load->view('admin/header');
		$this->load->view('admin/dashboard',$this->viewDate);
		$this->load->view('admin/footer');
	}
}
