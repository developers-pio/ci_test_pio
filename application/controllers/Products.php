
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {
	public $userData;
	public $viewData;

	function __construct(){
		parent::__construct();
		$this->userData = $this->session->userdata('userData');  
		$this->load->model(['User','Product']);
	}

	public function index(){
		$userid = empty($this->userData) ? null : $this->userData['id']; 
		$this->viewData['products'] = $this->Product->listing($userid);  
		$this->load->view('header');
		$this->load->view('product_index',$this->viewData);
		$this->load->view('footer');
	}

	public function addtocart(){ 
		if(empty($this->userData)){
			redirect('auth/login');
		} 
		if($this->input->post()){ 
			$this->load->library('form_validation');
			$this->form_validation->set_rules('qty','Quantity','required|is_natural_no_zero'); 
			$this->form_validation->set_rules('product_id','Product Id','required|is_natural_no_zero'); 
			if($this->form_validation->run() === true){ 
				if($this->input->post('action')=='add' && $this->input->post('qty') > 0){
					$this->db->where(['user_id'=>$this->userData['id'],'product_id'=>$this->input->post('product_id')])->delete('cart_items');
					$data = [
						'user_id'=>$this->userData['id'],
						'product_id'=>$this->input->post('product_id'),
						'qty'=>$this->input->post('qty'),
						'created_at'=>date('Y-m-d H:i:s')
					];
					$addtocart = $this->db->insert('cart_items',$data);
					if($addtocart){
						$this->session->set_flashdata('success','Product successfully added on cart.');   
					}else{
						$this->session->set_flashdata('error','Error in add to cart.');   
					} 
				}else if($this->input->post('action')=='remove'){
					$removefromcart = $this->db->where(['user_id'=>$this->userData['id'],'product_id'=>$this->input->post('product_id')])->delete('cart_items');
				    if($removefromcart){
						$this->session->set_flashdata('success','Product successfully remove from cart.');   
					}else{
						$this->session->set_flashdata('error','Error in remove cart.');   
					}  
				}
				
			} else{
				$this->session->set_flashdata('error',validation_errors());    
			}  
	   }
	   redirect('products');
	}
}
